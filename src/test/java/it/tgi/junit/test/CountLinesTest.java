package it.tgi.junit.test;

import it.tgi.test.CountLinesPlugin;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Created by Alessandro on 13/03/2017.
 */
public class CountLinesTest {

    private static final String TEST_FILE = "D:\\Projects\\intelliJ_prj\\PluginDemo\\CountLines\\src\\main\\java\\it\\tgi\\test\\CountLinesPlugin.java";

    /*
    * Verify the number of rows of the file CountLinesPlugin.java
    * NOTE: Remember to modify the value in case of modifies of the file
    */
    @Test
    public void testCount() {
        CountLinesPlugin p = new CountLinesPlugin();
        Long result = p.execute(new File(TEST_FILE));
        assertEquals("The file contains 46 rows", new Long(46), result);
    }
}
