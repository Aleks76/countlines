package it.tgi.test;

import org.plugface.DefaultPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;


/**
 * Created by Alessandro on 10/03/2017.
 */
public class CountLinesPlugin extends DefaultPlugin<File,Long> {

    public CountLinesPlugin() {
        super("countLine");
    }

    private static final Logger LOG = LoggerFactory.getLogger(CountLinesPlugin.class);

    public void start() { }

    public void stop() { }

    /**
     * This method return the number of rows in a txt file
     */
    public Long execute(File fileToRead) {

        long lineCounter = 0;
        try {
            FileReader fileStream = new FileReader(fileToRead);
            BufferedReader bufferedReader = new BufferedReader(fileStream);

            while (bufferedReader.readLine() != null)
                lineCounter++;

        } catch (FileNotFoundException ex) {
            LOG.error("File not found: {}", fileToRead);
        } catch (IOException ex) {
            LOG.error("There is an error: {}", ex);
        }
        LOG.debug("Plugin: " + this.getName().toUpperCase() + " --> The file contains " + lineCounter + " rows.");

        return lineCounter;
    }
}
